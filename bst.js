module.exports.Node = Node;
module.exports.BST = BST;

/*
	Constructor for the Node object
	@param data - Brick object 
	@param left - left child relationship
	@param right - right child relationship
*/
function Node(data, left, right) {
	this.data = data;
	this.left = left;
	this.right = right;
	this.show = show;
}

/*
	Returns the Node data (aka Brick object)
*/
function show() {
	return this.data;
}

/*
	Constructor for binary search tree object
*/
function BST() {
	this.root = null;
	this.insert = insert;
	this.find = find;
	this.count = count;
}

/*
	Inserts Brick object into appropriate place in the BST object
	@param data - Brick object to insert
*/
function insert(data) {
	var n = new Node(data, null, null);
	if (this.root == null){
		this.root = n;
	}
	else {
		var current = this.root;
		var parent;
		while (true) {
			parent = current;
			if (data.size < current.data.size) {
				current = current.left;
				if (current == null) {
					parent.left = n;
					break;
				}
			}
			else {
				current = current.right;
				if (current	== null) {
					parent.right = n;
					break;
				}
			}
		}
	}
}

/*
	Returns true if Brick of certain size is found in BST; false if not found
	@param size - Brick size to find
*/
function find(size) {
	var current = this.root;
	while (current && current.data.size != size) {

		if (size < current.data.size) {
			current = current.left;
		}
		else {
			current = current.right;
		}
	}
	return current != null;
}

/*
	Returns number of non null Node in BST object
*/
function count() {
	this.countSubtree = countSubtree;
	if (this.root != null) {
		return this.countSubtree(this.root);
	}
	else {
		return 0;
	}
}

/*
	Recursive helper method to count non null Nodes in BST
	left and right subtrees
	@param node - the Node in the BST
*/
function countSubtree(node) {
	var count = 1;
	if (node.left !== null) {
		count += countSubtree(node.left);	
	}
	if (node.right !== null) {
		count += countSubtree(node.right);
	}
	return count;
}