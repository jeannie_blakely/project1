module.exports.Brick = Brick;
module.exports.LegoPile = LegoPile;

var bst = require('./bst');
const BST = bst.BST;

/*
	Conststructor for Brick object
*/
function Brick(size, color) {
	this.size = size;
	this.color = color;
}

/*
	Constructor for LegoPile object; creates and initializes BST objects
	for each Brick color
*/
function LegoPile() {
	this.dictionary = {
		red: redBst = new BST(),
		green: greenBst = new BST(),
		blue: blueBst = new BST(),
		yellow: yellowBst = new BST(),
		black: blackBst = new BST(),
		white: whiteBst = new BST()
	};
	this.insert = insert;	
	this.hasBrick = hasBrick;
}

/*
	Inserts Brick oject into a LegoPile object; uses color property to 
	add Brick to appropriate BST
*/
function insert(brick) {
	switch (brick.color) {
		case "red":
			redBst.insert(brick);
			break;			
		case "green":
			greenBst.insert(brick);
			break;	
		case "blue":
			blueBst.insert(brick);
			break;			
		case "yellow":
			yellowBst.insert(brick);
			break;			
		case "black":
			blackBst.insert(brick);
			break;			
		case "white":
			whiteBst.insert(brick);
			break;
		default:
			console.log("This Lego color not supported");
	}
}

/*
	Determines if LegoPile has Brick of specific size and color.  Uses 
	BST find method to search color specific BST objects by size
*/
function hasBrick(size, color) {
	switch (color) {
		case "red":
			return redBst.find(size);
			break;			
		case "green":
			return greenBst.find(size);
			break;	
		case "blue":
			return blueBst.find(size);
			break;			
		case "yellow":
			return yellowBst.find(size);
			break;			
		case "black":
			return blackBst.find(size);
			break;			
		case "white":
			return whiteBst.find(size);
			break;
		default:
			console.log("This Lego color not supported");
	}
}