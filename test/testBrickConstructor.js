var assert = require('assert');
var lego = require('../legos');
const Brick = lego.Brick;

describe('Brick', function() {
	
	it('Brick object should return size', function(){
		var theBrick  = new Brick(4, "blue");
		assert.equal(theBrick.size, 4);
	});
	
	it('Brick object should return color', function(){
		var theBrick  = new Brick(4, "blue");
		assert.equal(theBrick.color, "blue");
	});
	
});