var assert = require('assert');
var lego = require('../legos');
var bst = require('../bst');

const Brick = lego.Brick;
const BST = bst.BST;


describe('BST', function() {
	it('BST#find() should return true with one Brick inserted', function(){
		var theBST = new BST();
		var theBrick = new Brick(4, "blue");
		theBST.insert(theBrick);
		assert.equal(theBST.find(4), true);
	});
	
	it('BST#find() should return true/false with multiple Bricks inserted', function(){
		var theBST = new BST();
		var theBrick = new Brick(12, "blue");
		var theBrick2 = new Brick(2, "blue");
		var theBrick3 = new Brick(5, "blue");
		var theBrick4 = new Brick(10, "blue");
		var theBrick5 = new Brick(6, "blue");
		var theBrick6 = new Brick(8, "blue");
		var theBrick7 = new Brick(1, "blue");
		var theBrick8 = new Brick(14, "blue");
		theBST.insert(theBrick);
		theBST.insert(theBrick2);
		theBST.insert(theBrick3);
		theBST.insert(theBrick4);
		theBST.insert(theBrick5);
		theBST.insert(theBrick6);
		theBST.insert(theBrick7);
		theBST.insert(theBrick8);
		assert.equal(theBST.find(12), true);
		assert.equal(theBST.find(18), false);
		assert.equal(theBST.find(10), true);
		assert.equal(theBST.find(1), true);
		assert.equal(theBST.find(0), false);
		assert.equal(theBST.find(4), false);
		assert.equal(theBST.find(5), true);
		assert.equal(theBST.find(2), true);		
	});

});