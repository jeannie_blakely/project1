var assert = require('assert');
var lego = require('../legos');
var bst = require('../bst');

const Brick = lego.Brick;
const BST = bst.BST;
const LegoPile = lego.LegoPile;

describe('LegoPile', function() {
	
	it('LegoPile#insert should insert one brick', function(){
		var theBrick  = new Brick(4, "blue");
		var theLegoPile = new LegoPile();
		theLegoPile.insert(theBrick);
		assert.equal(theLegoPile.dictionary["blue"].root.data, theBrick);
	});
	
	it('LegoPile#insert should insert one brick of each color', function(){
		var redBrick  = new Brick(6, "red");
		var greenBrick  = new Brick(12, "green");
		var blueBrick  = new Brick(4, "blue");
		var yellowBrick  = new Brick(4, "yellow");
		var blackBrick  = new Brick(4, "black");
		var whiteBrick  = new Brick(4, "white");
		var theLegoPile = new LegoPile();
		theLegoPile.insert(redBrick);
		theLegoPile.insert(greenBrick);
		theLegoPile.insert(blueBrick);
		theLegoPile.insert(yellowBrick);
		theLegoPile.insert(blackBrick);
		theLegoPile.insert(whiteBrick);
		assert.equal(theLegoPile.dictionary["red"].root.data, redBrick);
		assert.equal(theLegoPile.dictionary["green"].root.data, greenBrick);
		assert.equal(theLegoPile.dictionary["blue"].root.data, blueBrick);
		assert.equal(theLegoPile.dictionary["yellow"].root.data, yellowBrick);
		assert.equal(theLegoPile.dictionary["black"].root.data, blackBrick);
		assert.equal(theLegoPile.dictionary["white"].root.data, whiteBrick);
	});
	
	it('LegoPile#insert should insert several bricks of one color', function(){
		var blueBrick1  = new Brick(4, "blue");
		var blueBrick2  = new Brick(5, "blue");
		var blueBrick3  = new Brick(2, "blue");
		var theLegoPile = new LegoPile();
		theLegoPile.insert(blueBrick1);
		theLegoPile.insert(blueBrick2);
		theLegoPile.insert(blueBrick3);
		assert.equal(theLegoPile.dictionary["blue"].root.data, blueBrick1);
		assert.equal(theLegoPile.dictionary["blue"].root.right.data, blueBrick2);
		assert.equal(theLegoPile.dictionary["blue"].root.left.data, blueBrick3);
	});
});

