var assert = require('assert');
var lego = require('../legos');
var bst = require('../bst');

const Brick = lego.Brick;
const BST = bst.BST;
const LegoPile = lego.LegoPile;

describe('LegoPile', function() {
	
	it('LegoPile#hasBrick should return true with single brick', function(){
		var theBrick  = new Brick(4, "blue");
		var theLegoPile = new LegoPile();
		theLegoPile.insert(theBrick);
		assert.equal(theLegoPile.hasBrick(4, "blue"), true);
	});
	
	it('LegoPile#hasBrick should return true/false with one brick of each color', function(){
		var redBrick  = new Brick(6, "red");
		var greenBrick  = new Brick(12, "green");
		var blueBrick  = new Brick(4, "blue");
		var yellowBrick  = new Brick(4, "yellow");
		var blackBrick  = new Brick(8, "black");
		var whiteBrick  = new Brick(4, "white");
		var theLegoPile = new LegoPile();
		theLegoPile.insert(redBrick);
		theLegoPile.insert(greenBrick);
		theLegoPile.insert(blueBrick);
		theLegoPile.insert(yellowBrick);
		theLegoPile.insert(blackBrick);
		theLegoPile.insert(whiteBrick);
		assert.equal(theLegoPile.hasBrick(6, "red"), true);
		assert.equal(theLegoPile.hasBrick(5, "green"), false);
		assert.equal(theLegoPile.hasBrick(5, "blue"), false);
		assert.equal(theLegoPile.hasBrick(4, "yellow"), true);
		assert.equal(theLegoPile.hasBrick(8, "black"), true);
		assert.equal(theLegoPile.hasBrick(4, "white"), true);
	});
	
	it('LegoPile#hasBrick should return true/false with several bricks of one color', function(){
		var blueBrick1  = new Brick(4, "blue");
		var blueBrick2  = new Brick(5, "blue");
		var blueBrick3  = new Brick(2, "blue");
		var theLegoPile = new LegoPile();
		theLegoPile.insert(blueBrick1);
		theLegoPile.insert(blueBrick2);
		theLegoPile.insert(blueBrick3);
		assert.equal(theLegoPile.hasBrick(5, "blue"), true);
		assert.equal(theLegoPile.hasBrick(4, "blue"), true);
		assert.equal(theLegoPile.hasBrick(1, "blue"), false);
	});
});

