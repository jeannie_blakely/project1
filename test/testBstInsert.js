var assert = require('assert');
var lego = require('../legos');
var bst = require('../bst');

const Brick = lego.Brick;
const BST = bst.BST;


describe('BST', function() {
	it('BST#insert() should insert single node at root', function(){
		var theBST = new BST();
		var theBrick = new Brick(4, "blue");
		theBST.insert(theBrick);
		assert.equal(theBST.root.data, theBrick);
	});
	
	it('BST#insert() should insert root plus left child', function(){
		var theBST = new BST();
		var theBrick = new Brick(4, "blue");
		var theBrick2 = new Brick(2, "red");
		theBST.insert(theBrick);
		theBST.insert(theBrick2);
		assert.equal(theBST.root.data, theBrick);
		assert.equal(theBST.root.left.data, theBrick2);
	});
	
	it('BST#insert() should insert root plus right child', function(){
		var theBST = new BST();
		var theBrick = new Brick(4, "blue");
		var theBrick2 = new Brick(2, "red");
		var theBrick3 = new Brick(5, "green");
		theBST.insert(theBrick);
		theBST.insert(theBrick2);
		theBST.insert(theBrick3);
		assert.equal(theBST.root.data, theBrick);
		assert.equal(theBST.root.left.data, theBrick2);
		assert.equal(theBST.root.right.data, theBrick3);		
	});
	
	it('BST#insert() should insert root and right child with multiple nodes', function(){
		var theBST = new BST();
		var theBrick = new Brick(12, "blue");
		var theBrick2 = new Brick(2, "red");
		var theBrick3 = new Brick(5, "green");
		var theBrick4 = new Brick(10, "green");
		var theBrick5 = new Brick(6, "white");
		var theBrick6 = new Brick(8, "blue");
		var theBrick7 = new Brick(1, "green");
		var theBrick8 = new Brick(14, "green");
		theBST.insert(theBrick);
		theBST.insert(theBrick2);
		theBST.insert(theBrick3);
		theBST.insert(theBrick4);
		theBST.insert(theBrick5);
		theBST.insert(theBrick6);
		theBST.insert(theBrick7);
		theBST.insert(theBrick8);
		assert.equal(theBST.root.data, theBrick);
		assert.equal(theBST.root.left.data, theBrick2);
		assert.equal(theBST.root.right.data, theBrick8);		
	});

});